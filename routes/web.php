<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('video', function () {
    return view('videoYoutube');
});

Route::get('video-facebook', function () {
    return view('video');
});

Route::get('fuentes', function () {
    return view('fuentes');
});

Route::get('otravista', function () {
    return view('otravista');
});

Route::get('geo', function () {
    return view('geo');
});