<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="{{ URL::asset('css/app.css'); }} ">
</head>

<body>

    <!-- Load Facebook SDK for JavaScript -->
    <div id="fb-root"></div>

    <script async defer src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.2"></script>

    <!-- Your embedded video player code -->
    <div class="fb-video" data-href="https://fb.watch/63UQUvjQ4Q/" data-width="500" data-show-text="false">
        <div class="fb-xfbml-parse-ignore">
        </div>
    </div>

</body>

</html>