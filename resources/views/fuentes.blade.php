<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Fuentes</title>
    <link rel="stylesheet" href="{{ URL::asset('css/app.css'); }} ">
</head>

<body>
    <h1>Vista 1</h1>
    <p>
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Dicta ipsa maiores labore sed doloremque distinctio
        possimus quo voluptate, necessitatibus similique nulla quos sit vitae reiciendis ipsam fugiat et dignissimos
        ducimus!
    </p>
    <a href="otravista">Ver en otra vista</a>
</body>

</html>