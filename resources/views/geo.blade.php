<html>

<head>
    <meta name="viewport" content="initial-scale=1.0, width=device-width" />
    <script src="https://js.api.here.com/v3/3.1/mapsjs-core.js" type="text/javascript" charset="utf-8"></script>
    <script src="https://js.api.here.com/v3/3.1/mapsjs-service.js" type="text/javascript" charset="utf-8"></script>
</head>

<body>
    <div style="width: 640px; height: 480px" id="mapContainer"></div>
</body>

</html>


<script>
    var x = document.getElementById("demo");
    var latitude, longitude;

    getLocation();
    function getLocation() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showPosition);
        } else {
            x.innerHTML = "Geolocation is not supported by this browser.";
        }
    }
    function showPosition(position) {
        latitude = position.coords.latitude;
        longitude = position.coords.longitude;

        var platform = new H.service.Platform({
            'apikey': 'YG1xcaeaFGC5wPTUgvkpnowP_Ms7rqOL-ILW33bzLp8'
        });

        var maptypes = platform.createDefaultLayers();

        var map = new H.Map(
            document.getElementById('mapContainer'),
            maptypes.vector.normal.map,
            {
                zoom: 14,
                center: { lng: longitude, lat: latitude }
            });
        var marker = new H.map.Marker({ lat: latitude, lng: longitude });
        map.addObject(marker);
    }
</script>